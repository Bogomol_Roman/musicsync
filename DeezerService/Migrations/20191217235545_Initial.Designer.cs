﻿// <auto-generated />
using System;
using DeezerService.DataAccessLayer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DeezerService.Migrations
{
    [DbContext(typeof(DeezerContext))]
    [Migration("20191217235545_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DeezerService.Models.Album", b =>
                {
                    b.Property<long>("Id");

                    b.Property<long?>("ArtistId");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("ArtistId");

                    b.ToTable("Albums");
                });

            modelBuilder.Entity("DeezerService.Models.Artist", b =>
                {
                    b.Property<long>("Id");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Artists");
                });

            modelBuilder.Entity("DeezerService.Models.Track", b =>
                {
                    b.Property<long>("Id");

                    b.Property<long?>("AlbumId");

                    b.Property<long?>("ArtistId");

                    b.Property<bool>("IsExplicit");

                    b.Property<string>("Title");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("AlbumId");

                    b.HasIndex("ArtistId");

                    b.HasIndex("UserId");

                    b.ToTable("Tracks");
                });

            modelBuilder.Entity("DeezerService.Models.User", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("DeezerId");

                    b.Property<string>("DeezerPassword");

                    b.Property<string>("DeezerUsername");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("DeezerService.Models.Album", b =>
                {
                    b.HasOne("DeezerService.Models.Artist", "Artist")
                        .WithMany()
                        .HasForeignKey("ArtistId");
                });

            modelBuilder.Entity("DeezerService.Models.Track", b =>
                {
                    b.HasOne("DeezerService.Models.Album", "Album")
                        .WithMany()
                        .HasForeignKey("AlbumId");

                    b.HasOne("DeezerService.Models.Artist", "Artist")
                        .WithMany()
                        .HasForeignKey("ArtistId");

                    b.HasOne("DeezerService.Models.User")
                        .WithMany("SavedTracks")
                        .HasForeignKey("UserId");
                });
#pragma warning restore 612, 618
        }
    }
}
