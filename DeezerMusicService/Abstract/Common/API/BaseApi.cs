﻿using DeezerMusicService.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using YouTubeMusicService.Config.Result;

namespace DeezerMusicService.Abstract.Common.API
{
    public abstract class BaseApi
    {
        public virtual T GetObjectAsync<T>(string url, string requestMethod = "GET", List<Cookie> cookies = null)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = requestMethod;

                if (cookies != null)
                {
                    if (request.CookieContainer == null)
                    {
                        request.CookieContainer = new CookieContainer();
                    }

                    foreach (var cookie in cookies)
                        request.CookieContainer.Add(cookie);
                }

                var httpResponse = request.GetResponse();

                using (var stream = httpResponse.GetResponseStream())
                {
                    StreamReader stremReder = new StreamReader(stream);
                    string json = stremReder.ReadToEnd();


                    stremReder.Dispose();

                    if (IsParseErrorData(json))
                    {
                        //Logger.Logger.Log.Error(json);

                        return default(T);
                    }

                    return JsonConvert.DeserializeObject<T>(json);
                }

            }
            catch (Exception e)
            {
                //Logger.Logger.Log.Error(e);

                return default(T);
            }
        }




        public virtual DataResult<CreatePlayList> Post( string url, string title1)
        {
            try
            {

                WebRequest request = WebRequest.Create(url);
                request.Method = "POST"; 
                string title = $"title={title1}";
            
                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(title);
               
                request.ContentType = "application/x-www-form-urlencoded";
            
                request.ContentLength = byteArray.Length;

               
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
                string responseJson;

                WebResponse response = request.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        responseJson = reader.ReadToEnd();
                    }
                }

                DataResult<CreatePlayList> data = new DataResult<CreatePlayList>();

                data.Data = JsonConvert.DeserializeObject<CreatePlayList>(responseJson);
                data.Success = true;


                return data;

              
            }
            catch (Exception e)
            {

                return new DataResult<CreatePlayList>()
                {
                    Success = false

                };
                //Logger.Logger.Log.Error(e);

               // return default(T);
            }
        }

        public virtual T GetObjec<T>(string url, string requestMethod = "GET")
        {

            try
            {
                var request = WebRequest.Create(url);
                request.Method = requestMethod;

                var httpResponse = request.GetResponse();

                using (var stream = httpResponse.GetResponseStream())
                {
                    StreamReader stremReder = new StreamReader(stream);
                    string json = stremReder.ReadToEnd();


                    stremReder.Dispose();

                    if (IsParseErrorData(json))
                    {
                       // Logger.Logger.Log.Error(json);

                        return default(T);
                    }

                    return JsonConvert.DeserializeObject<T>(json);
                }

            }
            catch (Exception e)
            {
               // Logger.Logger.Log.Error(e);

                return default(T);
            }
        }

        public virtual void Send(string url)
        {
            try
            {
                var request = WebRequest.Create(url);
                request.Method = "GET";

                var httpResponse = request.GetResponse();
            }
            catch (Exception e)
            {
               // Logger.Logger.Log.Error(e);
            }
        }

        protected virtual bool IsParseErrorData(string json)
        {
            try
            {
                var errorJson = JObject.Parse(json);
                var errorToken = errorJson["error"];

                return errorToken != null;
            }
            catch
            {
                return false;
            }
        }
    }
}
