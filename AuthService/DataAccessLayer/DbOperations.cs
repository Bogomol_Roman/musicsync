﻿using System.Linq;
using AuthService.Models;

namespace AuthService.DataAccessLayer
{
    public class DbOperations
    {
        public UserContext _context;
        public DbOperations(UserContext context)
        {
            _context =  context;
        }
        
        public User GetUserByCredentials(string username, string password)
        {
            var result = _context.User.FirstOrDefault(u => u.Username == username && u.Password == password);
            return result;
        }
    }
}