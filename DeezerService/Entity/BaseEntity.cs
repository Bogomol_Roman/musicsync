﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeezerService.Entity
{
    public class BaseEntity
    {
    }

    public class Rootobject
    {
        public Datum[] data { get; set; }
        public string checksum { get; set; }
        public int total { get; set; }
    }

    public class Datum
    {
        public string id { get; set; }
        public string title { get; set; }
        public int duration { get; set; }
        public bool _public { get; set; }
        public bool is_loved_track { get; set; }
        public bool collaborative { get; set; }
        public int nb_tracks { get; set; }
        public int fans { get; set; }
        public string link { get; set; }
        public string picture { get; set; }
        public string picture_small { get; set; }
        public string picture_medium { get; set; }
        public string picture_big { get; set; }
        public string picture_xl { get; set; }
        public string checksum { get; set; }
        public string tracklist { get; set; }
        public string creation_date { get; set; }
        public int time_add { get; set; }
        public int time_mod { get; set; }
        public Creator creator { get; set; }
        public string type { get; set; }
    }

    public class Creator
    {
        public string id { get; set; }
        public string name { get; set; }
        public string tracklist { get; set; }
        public string type { get; set; }
    }




    public class RootobjectUser
    {
        public string id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string email { get; set; }
        public int status { get; set; }
        public string birthday { get; set; }
        public string inscription_date { get; set; }
        public string gender { get; set; }
        public string link { get; set; }
        public string picture { get; set; }
        public string picture_small { get; set; }
        public string picture_medium { get; set; }
        public string picture_big { get; set; }
        public string picture_xl { get; set; }
        public string country { get; set; }
        public string lang { get; set; }
        public bool is_kid { get; set; }
        public string explicit_content_level { get; set; }
        public string[] explicit_content_levels_available { get; set; }
        public string tracklist { get; set; }
        public string type { get; set; }
    }

}
