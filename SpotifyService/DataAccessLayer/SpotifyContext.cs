﻿using Microsoft.EntityFrameworkCore;
using SpotifyService.Models;

namespace SpotifyService.DataAccessLayer
{
    public class SpotifyContext : DbContext
    {
        public SpotifyContext(DbContextOptions options) :base(options){}
        public DbSet<Album> Albums { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Track> Tracks { get; set; }
        public DbSet<User> Users { get; set; }
        
    }
}