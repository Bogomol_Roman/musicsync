﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouTubeMusicService.Config.Result
{
    public class Result
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
