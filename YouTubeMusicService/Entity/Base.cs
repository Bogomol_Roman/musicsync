﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouTubeMusicService.Entity
{
    public class Base
    {
        public string Name { get; set; }
    }

    public class YouTubeTrack : Base
    {
        public string Id { get; set; }
        public string ArtistName { get; set; }
        public string Album { get; set; }
        public string Duration { get; set; }

        public override string ToString()
        {
            return $"{Name} - {Duration}";
        }
    }

    public class YouTubeArtist : YouTubeBase
    {
        public string Id { get; set; }
    }


    public class YouTubePlayList : YouTubeBase
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public bool IsVisibl { get; set; }
    }

   

    public class YouTubeBase : Base
    {
        private List<YouTubeTrack> _tracks;


        public List<YouTubeTrack> Tracks
        {
            get
            {
                if (_tracks == null) _tracks = new List<YouTubeTrack>();
                return _tracks;
            }
            set
            {
                _tracks = value;
            }
        }
    }


    public class TrackInfo 
    {
        public string Name { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public string Duration { get; set; }
        public string Url { get; set; }
        public string NameArtist { get; set; }
    }
}
