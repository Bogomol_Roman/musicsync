﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeezerMusicService.Repository;
using Microsoft.AspNetCore.Mvc;

namespace DeezerMusicService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{access_token}")]
        public void Get(string access_token)
        {
            new DeezerRepository().GetAllPlayList(access_token);
            new DeezerRepository().GetUser(access_token);
        }


        //[HttpGet("{access_token}")]
        //public void GetAllTrack(string access_token)
        //{
        //    new DeezerRepository().GetAllPlayList(access_token);
        //}


        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
