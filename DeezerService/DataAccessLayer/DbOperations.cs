﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeezerService.Models;
using E.Deezer;
using Microsoft.EntityFrameworkCore;

namespace DeezerService.DataAccessLayer
{
    public class DbOperations
    {
        private DeezerContext _context;

        public DbOperations(DeezerContext context)
        {
            _context = context;
        }

        public IEnumerable<Track> GetAllTracks()
        {
            return _context.Tracks.Include("Artist").Include("Album").ToList();
        }

        public async Task SaveAllTracks(IEnumerable<Track> tracks)
        {
            foreach (var track in tracks)
            {
                if (_context.Tracks.Contains(track))
                {
                    await _context.Tracks
                        .Upsert(track)
                        .NoUpdate()
                        .RunAsync();
                }
                else
                {
                    _context.Tracks.Add(track);
                    if (_context.Albums.Contains(track.Album))
                    {
                        _context.Albums.Remove(track.Album);
                    }
                    if (_context.Artists.Contains(track.Artist))
                    {
                        _context.Artists.Remove(track.Artist);
                    }
                }
            }
            await _context.SaveChangesAsync();
        }
    }
}