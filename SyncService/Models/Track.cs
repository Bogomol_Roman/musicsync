﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SynchroService.Models
{
    public class Track
    {
        public string Id { get; set; }

        public string Title { get; set; }
        public Artist Artist { get; set; }
        public Album Album { get; set; }
        public bool IsExplicit { get; set; }
    }
}