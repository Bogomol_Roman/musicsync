﻿using System.Threading.Tasks;
using DeezerService.DataAccessLayer;
using DeezerService.Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DeezerService.Controllers
{
    //   [Authorize(AuthenticationSchemes = "MusicSync")]
    [Route("api/[controller]")]
    [ApiController]
    public class DeezerController : Controller
    {
        private DeezerLogic _logic;

        public DeezerController(IConfiguration configuration, DeezerContext context)
        {
            _logic = new DeezerLogic(configuration, context);
        }

        [HttpGet("get-tracks")]
        public JsonResult GetAllUserSavedTracks()
        {
            return Json(2);
        }

        [HttpGet("force-get-tracks")]
        public async Task<JsonResult> ForceGetAllUserSavedTracks()
        {
            var tracks = await _logic.ForceGetTracks();

            return Json(JsonConvert.SerializeObject(tracks, Formatting.None));
        }

   


       

    }
}