﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeezerMusicService.Abstract.Common.Config
{
    public static class DeezerAccessProperties
    {
        public static string BaseUrl { get; set; } = "https://api.deezer.com/";
    }
}
