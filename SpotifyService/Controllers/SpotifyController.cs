﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SpotifyService.DataAccessLayer;
using SpotifyService.Logic;

namespace SpotifyService.SpotifyHelper
{
    //[Authorize(AuthenticationSchemes = "MusicSync")]
    [Route("api/[controller]")]
    [ApiController]
    public class SpotifyController : Controller
    {
        private SpotifyLogic _logic;

        public SpotifyController(IConfiguration configuration, SpotifyContext context)
        {
            _logic = new SpotifyLogic(configuration, context);
        }

        [HttpGet("get-tracks")]
        public JsonResult GetAllUserSavedTracks()
        {
            var tracks = _logic.GetTracksForUser();
            
            return Json(JsonConvert.SerializeObject(tracks, Formatting.None));
        }

        [HttpGet("force-get-tracks")]
        public async Task<JsonResult> ForceGetAllUserSavedTracks()
        {
            var tracks = await _logic.ForceGetTracks();

            return Json(JsonConvert.SerializeObject(tracks, Formatting.None));
        }
    }
}