﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SpotifyService.DataAccessLayer;
using SpotifyService.Models;
using SpotifyService.SpotifyHelper;

namespace SpotifyService.Logic
{
    public class SpotifyLogic
    {
        private SpotifyClient _client;
        private DbOperations _database;

        public SpotifyLogic(IConfiguration configuration, SpotifyContext context)
        {
            _client = new SpotifyClient(configuration);
            _database = new DbOperations(context);
        }

        public async Task SaveTracksForUser(IEnumerable<Track> tracks)
        {
            await _database.SaveAllTracks(tracks);
        }

        public IEnumerable<Track> GetTracksForUser()
        {
            var tracks = Enumerable.Empty<Track>();

            tracks = _database.GetAllTracks();
            return tracks;
        }

        public async Task<IEnumerable<Track>> ForceGetTracks()
        {
            await _client.AuthAsync();
            var trackList = await _client.GetUserTracks();
            await SaveTracksForUser(trackList);
            return trackList;
        }
    }
}