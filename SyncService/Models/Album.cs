﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SynchroService.Models
{
    public class Album
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public Artist Artist { get; set; }
    }
}