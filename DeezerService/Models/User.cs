﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeezerService.Models
{
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        
        public string Id { get; set; }
        public int DeezerId { get; set; }
        public string DeezerUsername { get; set; }
        public string DeezerPassword { get; set; }
        public IEnumerable<Track> SavedTracks { get; set; }
    }
}