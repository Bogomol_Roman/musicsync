﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeezerService.Common.Config
{
    public class DeezerAccessProperties
    {
        public string BaseUrl { get; set; } = "https://api.deezer.com/";
    }
}
