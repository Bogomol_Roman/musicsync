﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeezerMusicService.Entity;
using DeezerMusicService.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using YouTubeMusicService.Config.Result;

namespace DeezerMusicService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeezerController : Controller
    {

        [HttpGet("{access_token}")]
        [Route("GetUser")]
        public DataResult<RootobjectUser> GetUser(string access_token)
        {
           return  new DeezerRepository().GetUser(access_token);
        }

        
        [Route("GetPlayLists")]
        public DataResult<RootobjectPlayList> GetPlayLists(string access_token)
        {
            return new DeezerRepository().GetAllPlayList(access_token);
        }

        [Route("CreatePlayList")]
        public DataResult<CreatePlayList> CreatePlayList(string access_token, string title)
        {
            return  new DeezerRepository().CreatePlayList(access_token, title);
        }
    }
}