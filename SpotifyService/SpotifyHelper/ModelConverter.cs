﻿using System.Collections.Generic;
using System.Linq;
using SpotifyService.Models;
using Spotify.API.NetCore;
using Spotify.API.NetCore.Models;

namespace SpotifyService.SpotifyHelper
{
    internal static class ModelConverter
    {
        public static Track ConvertExternalToInternalTrackModel(FullTrack extTrackModel)
        {
            var trackArtist = ConvertExternalToInternalArtistModel(extTrackModel.Artists.FirstOrDefault());
            var trackAlbum = ConvertExternalToInternalAlbumModel(extTrackModel.Album);

            trackAlbum.Artist = trackArtist;

            return new Track
            {
                Id = extTrackModel.Id,
                Title = extTrackModel.Name,
                Artist = trackArtist,
                Album = trackAlbum,
                IsExplicit = extTrackModel.Explicit
            };
        }

        public static ICollection<Artist> ConvertExternalToInternalArtistListModel(IEnumerable<SimpleArtist> artists)
        {
            return artists.Select(ConvertExternalToInternalArtistModel).ToList();
        }

        public static Artist ConvertExternalToInternalArtistModel(SimpleArtist extArtist)
        {
            return new Artist
            {
                Id = extArtist.Id,
                Name = extArtist.Name
            };
        }

        public static Album ConvertExternalToInternalAlbumModel(SimpleAlbum extAlbum)
        {
            return new Album
            {
                Id = extAlbum.Id,
                Title = extAlbum.Name
            };
        }

        public static User ConvertExternalToInternalUserModel(PrivateProfile extUser)
        {
            return new User
            {
                SpotifyId = extUser.Id
            };
        }
    }
}