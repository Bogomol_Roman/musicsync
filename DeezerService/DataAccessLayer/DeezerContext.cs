﻿using DeezerService.Models;
using Microsoft.EntityFrameworkCore;

namespace DeezerService.DataAccessLayer
{
    public class DeezerContext: DbContext
    {
        public DeezerContext(DbContextOptions options) :base(options){}
        public DbSet<Album> Albums { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Track> Tracks { get; set; }
        public DbSet<User> Users { get; set; }
        
    }
}