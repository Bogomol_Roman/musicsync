﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Spotify.API.NetCore;
using Spotify.API.NetCore.Auth;
using Spotify.API.NetCore.Enums;
using SpotifyService.DataAccessLayer;
using SpotifyService.Models;

namespace SpotifyService.SpotifyHelper
{
    public class SpotifyClient
    {
        private static SpotifyWebAPI _spotifyApiClient;

        private readonly string _clientId;
        private readonly string _redirectUrl;

        public SpotifyClient(IConfiguration configuration)
        {
            _clientId = configuration["SpotifyAppSettings:ClientId"];
            _redirectUrl = configuration["SpotifyAppSettings:RedirectUrl"];
        }

        public async Task AuthAsync()
        {
            if (_spotifyApiClient == null | _spotifyApiClient?.AccessToken == null)
            {
                var factory = new WebAPIFactory(_redirectUrl, 80,
                    _clientId, Scope.UserLibraryRead);
                _spotifyApiClient = await factory.GetWebApi();
            }
        }

        public async Task<IEnumerable<Track>> GetUserTracks()
        {
            var trackList = new List<Track>();
            var result = await _spotifyApiClient.GetSavedTracksAsync(200);
            //           while (result.HasNextPage())
//            {
            trackList.AddRange(result.Items.Select(track =>
                ModelConverter.ConvertExternalToInternalTrackModel(track.Track)));
            //}

            return trackList;
        }

        public User GetUserInfo()
        {
            var profile = _spotifyApiClient.GetPrivateProfile();
            return ModelConverter.ConvertExternalToInternalUserModel(profile);
        }
    }
}