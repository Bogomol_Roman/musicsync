﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SynchroService.Models;

namespace SynchroService.Logic
{
    public class SyncLogic
    {
        private readonly string _token;
        private readonly string _spotifyEndpoint;
        private readonly string _deezerEndpoint;
        
        public SyncLogic(IConfiguration configuration)
        {
            _token = configuration["InnerToken"];
            _token = configuration["SpotifyEndpoint"];
            _token = configuration["DeezerEndpoint"];
        }
        
        public async Task<IEnumerable<Track>> GetCombinedTrackList()
        {
            var spotifyTracks = await ReceiveTracksFromSpotify();
            var deezerTracks = await ReceiveTracksFromDeezer();
            var resultList = Enumerable.Empty<Track>();

            if (spotifyTracks != null && deezerTracks != null)
            {
                resultList = spotifyTracks.Concat(deezerTracks).GroupBy(k => new {k.Title, k.Artist, k.Album})
                    .Select(g => g.First()).ToList();
            }

            return resultList;
        }

        private async Task<IEnumerable<Track>> ReceiveTracksFromSpotify()
        {
            List<Track> responseList;
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", _token);
                using (var response = await httpClient.GetAsync($"{_spotifyEndpoint}/get-tracks"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    var jsonResult = JsonConvert.DeserializeObject(apiResponse).ToString();
                    responseList = JsonConvert.DeserializeObject<List<Track>>(jsonResult, 
                        new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        });
                }
            }

            return responseList;
        }

        private async Task<IEnumerable<Track>> ReceiveTracksFromDeezer()
        {
            List<Track> responseList;
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", _token);
                using (var response = await httpClient.GetAsync($"{_deezerEndpoint}/get-tracks"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    var jsonResult = JsonConvert.DeserializeObject(apiResponse).ToString();
                    responseList = JsonConvert.DeserializeObject<List<Track>>(jsonResult, 
                        new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        });
                }
            }

            return responseList;
        }
    }
}