﻿using AuthService.Models;
using Microsoft.EntityFrameworkCore;

namespace AuthService.DataAccessLayer
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions options) :base(options){}
        public DbSet<User> User { get; set; }
    }
}