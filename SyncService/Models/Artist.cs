﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SynchroService.Models
{
    public class Artist
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}