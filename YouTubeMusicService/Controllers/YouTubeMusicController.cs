﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using YouTubeMusicService.Config.Result;
using YouTubeMusicService.Entity;
using YouTubeMusicService.Repository;

namespace YouTubeMusicService.Controllers
{

    

    [Route("api/[controller]")]
    [ApiController]
    public class YouTubeMusicController : ControllerBase
    {
        [HttpPost]
        [Route("PostYouTube")]
        public Task<List<YouTubePlayList>> GetPlayList([FromBody] GoogleToken value)
        {
            return new YouTubeRepository().GetPlayList(value);
        }


        [HttpPost]
        [Route("CreatePlayList")]
        public DataResult<List<YouTubeTrack>> CreatePlayList([FromBody] PlayListDataToCreate value)
        {
            return new YouTubeRepository().GetSearch(value.Title,value.youTubeTracks, value.googleToken);
        }

        [Route("post")]
        public string TestPost(string str)
        {
            return str;
        }


        //[HttpGet]
        //[Route("Get")]
        //public ActionResult<IEnumerable<string>> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}
    }
}