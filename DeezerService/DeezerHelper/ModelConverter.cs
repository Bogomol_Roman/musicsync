﻿using DeezerService.Models;
using E.Deezer.Api;

namespace DeezerService.DeezerHelper
{
    internal static class ModelConverter
    {
        public static Track ConvertExternalToInternalTrackModel(ITrack extTrackModel)
        {
            var trackArtist = ConvertExternalToInternalArtistModel(extTrackModel.Artist);
            var trackAlbum = ConvertExternalToInternalAlbumModel(extTrackModel.Album);

            trackAlbum.Artist = trackArtist;
            
            return new Track
            {
                Id = (long) extTrackModel.Id,
                Title = extTrackModel.Title,
                Artist = trackArtist,
                Album = trackAlbum,
                IsExplicit = extTrackModel.IsExplicit
            };
        }

        public static Artist ConvertExternalToInternalArtistModel(IArtist extArtist)
        {
            return new Artist
            {
                Id = (long) extArtist.Id,
                Name = extArtist.Name
            };
        }
        
        public static Album ConvertExternalToInternalAlbumModel(IAlbum extAlbum)
        {
            return new Album
            {
                Id = (long) extAlbum.Id,
                Title = extAlbum.Title,
            };
        }
    }
}