﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeezerService.DataAccessLayer;
using DeezerService.DeezerHelper;
using DeezerService.Models;
using Microsoft.Extensions.Configuration;

namespace DeezerService.Logic
{
    public class DeezerLogic
    {
        private DeezerClient _client;
        private DbOperations _database;

        public DeezerLogic(IConfiguration configuration, DeezerContext context)
        {
            _client = new DeezerClient(configuration);
            _database = new DbOperations(context);
        }

        public async Task SaveTracksForUser(IEnumerable<Track> tracks)
        {
            await _database.SaveAllTracks(tracks);
        }

        public IEnumerable<Track> GetTracksForUser()
        {
            var tracks = Enumerable.Empty<Track>();

            tracks = _database.GetAllTracks();
            return tracks;
        }

        public async Task<IEnumerable<Track>> ForceGetTracks()
        {
            var trackList = await _client.GetCurrentUserTracks();
            await SaveTracksForUser(trackList);
            return trackList;
        }
    }
}