﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouTubeMusicService.Repository;

namespace YouTubeMusicService.Entity
{
    public class PlayListDataToCreate
    {
        public GoogleToken googleToken { get; set; }
        public List<YouTubeTrack> youTubeTracks { get; set; }
        public string Title { get; set; }
    }
}
