﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpotifyService.Models
{
    public class User
    {
        public int Id { get; set; }
        public string SpotifyId { get; set; }
        public ICollection<Track> SavedTracks { get; set; }
    }
}