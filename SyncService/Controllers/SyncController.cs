﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SynchroService.Logic;

namespace SynchroService.Controllers
{
    //[Authorize(AuthenticationSchemes = "MusicSync")]
    [Route("api/[controller]")]
    [ApiController]
    public class SyncController : Controller
    {
        private readonly SyncLogic _logic;

        public SyncController(IConfiguration configuration)
        {
            _logic = new SyncLogic(configuration);
        }
        
        [HttpGet("get-tracks")]
        public async Task<JsonResult> GetAllUserSavedTracks()
        {
            var tracks = await  _logic.GetCombinedTrackList();
            
            return Json(JsonConvert.SerializeObject(tracks, Formatting.None));
        }
    }
}