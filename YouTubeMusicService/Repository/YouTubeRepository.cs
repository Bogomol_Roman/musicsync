﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using Google.Apis.YouTube.v3.Data;
using Google.Apis.YouTube.v3;
using Google.Apis.Auth.OAuth2;
using System.IO;
using YouTubeMusicService.Entity;
using YouTubeMusicService.Config.Result;
using Google.Apis.Auth.OAuth2.Flows;
using Newtonsoft.Json;

namespace YouTubeMusicService.Repository
{


    public static class AccessProperties
    {
        public static string UserId { get; set; }
        public static YouTubeService YouTubeService { get; set; }
        public static Dictionary<string, System.Net.Cookie> AllCookies { get; set; }
    }

    public class GoogleToken
    {
        [JsonProperty("access_token")]
        public string Token { get; set; }
        [JsonProperty("refresh_token")]
        public string RefrechToken { get; set; }
        [JsonProperty("id_token")]
        public string IdToken { get; set; }
    }

    public class YouTubeRepository
    {



        public async void Auth(YouTubeMusicService.Repository.GoogleToken googleToken)
        {
            UserCredential credential = null;
            UserCredential rredential = null;


            using (var stream = new FileStream("client_id.json", FileMode.Open, FileAccess.Read))
            {
                credential = new UserCredential(new GoogleAuthorizationCodeFlow(
                new GoogleAuthorizationCodeFlow.Initializer
                {
                    ClientSecrets = GoogleClientSecrets.Load(stream).Secrets,
                    Scopes = new string[] { YouTubeService.Scope.Youtube },
                    DataStore = new FileDataStore(this.GetType().ToString())

                }),
             "user",
             new Google.Apis.Auth.OAuth2.Responses.TokenResponse()
             {
                 AccessToken = googleToken.Token,
                 RefreshToken = googleToken.RefrechToken,
                 IdToken = googleToken.IdToken
             });
            }



            YouTubeService youtube = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "ImportMusic",
            });

            AccessProperties.YouTubeService = youtube;

            //return GetPlayList();
        }


        public async Task<List<YouTubePlayList>> GetPlayList(YouTubeMusicService.Repository.GoogleToken googleToken)
        {
            Auth(googleToken);
            string pageToken = null;
            List<YouTubePlayList> youTubePlayLists = new List<YouTubePlayList>();

            do
            {
                PlaylistsResource.ListRequest playlistListResponse =
                    AccessProperties.YouTubeService.Playlists.List("snippet,contentDetails");
                playlistListResponse.Mine = true;
                playlistListResponse.PageToken = pageToken;
                playlistListResponse.MaxResults = 50;
                PlaylistListResponse res = playlistListResponse.Execute();
                pageToken = res.NextPageToken;
                youTubePlayLists.AddRange(res.Items.Select(p => new YouTubePlayList()
                {
                    Name = p.Snippet.Title,
                    Id = p.Id

                }));
            } while (pageToken != null);


            for (int i = 0; i < youTubePlayLists.Count; i++)
            {
                youTubePlayLists[i].Tracks.AddRange(GetTracksByPlayList(youTubePlayLists[i].Id));
            }

            return youTubePlayLists;
        }

        public async Task<List<YouTubeTrack>> GetTracks()
        {
            string pageToken = null;
            List<YouTubeTrack> youTubeTracks = new List<YouTubeTrack>();

            do
            {
                VideosResource.ListRequest videosListMyRatedVideosRequest =
                    AccessProperties.YouTubeService.Videos.List("snippet,contentDetails");

                videosListMyRatedVideosRequest.MyRating =
                    VideosResource.ListRequest.MyRatingEnum.Like;
                videosListMyRatedVideosRequest.MaxResults = 50;

                if (pageToken != null)
                {
                    videosListMyRatedVideosRequest.PageToken = pageToken;
                }

                VideoListResponse response =
                    await videosListMyRatedVideosRequest.ExecuteAsync();
                pageToken = response.NextPageToken;
                var tempList = response.Items.Where(p => p.Snippet != null &&
                p.Snippet.CategoryId == "10").Select(p => new YouTubeTrack()
                {
                    Name = p.Snippet?.Title,
                    Id = p.Id,
                    Duration = p.ContentDetails.Duration
                });

                youTubeTracks.AddRange(tempList);

            } while (pageToken != null);

            return youTubeTracks;
        }

        public async Task<List<YouTubeArtist>> GetYouTubeArtists()
        {
            List<YouTubeArtist> youTubeArtists =
                new List<YouTubeArtist>();
            string pageToken = null;
            do
            {
                SubscriptionsResource.ListRequest artists = AccessProperties
                .YouTubeService.Subscriptions.List("snippet,contentDetails");
                artists.Mine = true;
                artists.MaxResults = 50;
                artists.PageToken = pageToken;
                SubscriptionListResponse response =
                    await artists.ExecuteAsync();
                pageToken = response.NextPageToken;
                youTubeArtists.AddRange(response.Items.Select(p => new YouTubeArtist()
                {
                    Id = p.Snippet.ChannelId,
                    Name = p.Snippet.Title
                }));

            } while (pageToken != null);

            return youTubeArtists;
        }

        public List<YouTubeTrack> GetTracksByPlayList(string id)
        {
            string pageToken = null;
            List<YouTubeTrack> youTubeTracks = new List<YouTubeTrack>();

            do
            {
                PlaylistItemsResource.ListRequest videosListMyRatedVideosRequest =
                    AccessProperties.YouTubeService.PlaylistItems.List("snippet,contentDetails");
                videosListMyRatedVideosRequest.PlaylistId = id;
                videosListMyRatedVideosRequest.MaxResults = 50;

                if (pageToken != null)
                {
                    videosListMyRatedVideosRequest.PageToken = pageToken;
                }

                PlaylistItemListResponse response = videosListMyRatedVideosRequest.Execute();
                pageToken = response.NextPageToken;
                var tempList = response.Items.Where(p => p.Snippet?.Title != null &&
                p.Snippet.Title != "Private video").Select(p => new YouTubeTrack()
                {
                    Name = p.Snippet?.Title,
                    Id = p.ContentDetails.VideoId
                }).ToList();


                youTubeTracks.AddRange(tempList);


            } while (pageToken != null);

            return youTubeTracks;
        }


        public DataResult<List<YouTubeTrack>> GetSearch(string title, List<YouTubeTrack> search, GoogleToken googleToken)
        {
            Auth(googleToken);
            List<YouTubeTrack> youTubeTracks = new List<YouTubeTrack>();

            SearchResource.ListRequest videosListMyRatedVideosRequest =
                AccessProperties.YouTubeService.Search.List("snippet");


            DataResult<List<YouTubeTrack>> data = new DataResult<List<YouTubeTrack>>();
            data.Data = new List<YouTubeTrack>();
            try
            {
                foreach (var item in search)
                {
                    videosListMyRatedVideosRequest.Q = item.Name;
                    videosListMyRatedVideosRequest.MaxResults = 50;
                    SearchListResponse response = videosListMyRatedVideosRequest.Execute();
                    data.Data.Add(ChekingMusic(response).First());
                }

                if (title != string.Empty)
                {
                    AddPlayList(title, data.Data, googleToken);
                }

                data.Success = true;

            }
            catch (Exception ex)
            {
                data.Success = false;
                data.Message = ex.Message;
            }

            return data;
        }

        private List<YouTubeTrack> ChekingMusic(SearchListResponse searchList)
        {
            VideosResource.ListRequest videosListMyRatedVideosRequest =
           AccessProperties.YouTubeService.Videos.List("snippet,contentDetails");
            videosListMyRatedVideosRequest.Id = searchList.Items
                .Select(p => p.Id.VideoId)
                .Aggregate((i, j) => i + ',' + j);
            videosListMyRatedVideosRequest.MaxResults = 1;
            VideoListResponse response = videosListMyRatedVideosRequest.Execute();

            return response.Items.Where(p => p.Snippet != null &&
            p.Snippet.CategoryId == "10").Select(p => new YouTubeTrack()
            {
                Id = p.Id,
                Name = p.Snippet.Title,
            }).ToList();
        }



        public async void AddPlayList(string title, List<YouTubeTrack> youTubeTracks, GoogleToken googleToken)
        {

            PlaylistItem playlistItem = new PlaylistItem();
            PlaylistItemSnippet snippet = new PlaylistItemSnippet();
            ResourceId resourceId = new ResourceId();
            List<YouTubePlayList> playLists = await GetPlayListAsync();

            if (playLists.Any(p => p.Name.ToLower().Equals(title.ToLower())))
            {
                snippet.PlaylistId = playLists.FirstOrDefault
                    (p => p.Name.ToLower().Equals(title.ToLower())).Id;
            }
            else
            {
                snippet.PlaylistId = CreatePlayList(title).Data;
            }

            foreach (var item in youTubeTracks
              .Select(p => p.Id))
            {
                resourceId.VideoId = item;
                resourceId.Kind = "youtube#video";
                snippet.ResourceId = resourceId;
                playlistItem.Snippet = snippet;

                var playlistItemsInsertRequest = AccessProperties.YouTubeService
                     .PlaylistItems.Insert(playlistItem, "snippet");
                playlistItemsInsertRequest.Execute();
            }
        }


        public async Task<List<YouTubePlayList>> GetPlayListAsync()
        {
            string pageToken = null;
            List<YouTubePlayList> youTubePlayLists = new List<YouTubePlayList>();

            do
            {
                PlaylistsResource.ListRequest playlistListResponse =
                    AccessProperties.YouTubeService.Playlists.List("snippet,contentDetails");
                playlistListResponse.Mine = true;
                playlistListResponse.PageToken = pageToken;
                playlistListResponse.MaxResults = 50;
                PlaylistListResponse res = await playlistListResponse.ExecuteAsync();
                pageToken = res.NextPageToken;
                youTubePlayLists.AddRange(res.Items.Select(p => new YouTubePlayList()
                {
                    Name = p.Snippet.Title,
                    Id = p.Id

                }));
            } while (pageToken != null);

            return youTubePlayLists;
        }

        public DataResult<string> CreatePlayList(string title)
        {

            Playlist playlist = new Playlist();
            PlaylistSnippet snippet = new PlaylistSnippet();
            snippet.Title = title;
            PlaylistStatus status = new PlaylistStatus();
            playlist.Snippet = snippet;
            playlist.Status = status;

            var playlistsInsertRequest = AccessProperties.YouTubeService.
                Playlists.Insert(playlist, "snippet,status");
            DataResult<string> dataResult = new DataResult<string>();


            Playlist f = playlistsInsertRequest.Execute();

            dataResult.Data = f.Id;


            if (dataResult.Data == string.Empty || dataResult.Data == null)
            {
                dataResult.Success = false;

            }
            else
            {
                dataResult.Success = true;
            }
            
            return dataResult;
        }




    }
}
