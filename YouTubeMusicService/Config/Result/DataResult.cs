﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouTubeMusicService.Config.Result
{
    public class DataResult<T> : Result
    {
        public T Data { get; set; }
    }
}
