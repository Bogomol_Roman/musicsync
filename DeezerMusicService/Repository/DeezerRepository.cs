﻿using DeezerMusicService.Abstract.Common.API;
using DeezerMusicService.Abstract.Common.Config;
using DeezerMusicService.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using YouTubeMusicService.Config.Result;

namespace DeezerMusicService.Repository
{
    public class DeezerRepository : BaseApi
    {
        public DataResult<RootobjectUser> GetUser(string access_token)
        {
            string token = access_token;

            string url = $"{DeezerAccessProperties.BaseUrl}" + $"user/me?access_token={token}";
            DataResult<RootobjectUser> dataResult = new DataResult<RootobjectUser>();
            string json = string.Empty;

            try
            {
                var request = WebRequest.Create(url);
                request.Method = "GET";

                var httpResponse = request.GetResponse();

                using (var stream = httpResponse.GetResponseStream())
                {
                    StreamReader stremReder = new StreamReader(stream);
                    json = stremReder.ReadToEnd();


                    stremReder.Dispose();

                    if (IsParseErrorData(json))
                    {
                      //  dataResult.Data = rootobject;
                        dataResult.Success = true;
                        dataResult.Message = json;

                        return dataResult;
                        // Logger.Logger.Log.Error(json);

                        //  return default(T);
                    }

                   
                    RootobjectUser rootobject = JsonConvert.DeserializeObject<RootobjectUser>(json);

                    dataResult.Data = rootobject;
                    dataResult.Success = true;

                    return dataResult;
                    //return JsonConvert.DeserializeObject<T>(json);
                }

              

            }
            catch (Exception e)
            {
                dataResult.Success = false;
                dataResult.Message = e.Message;
                // Logger.Logger.Log.Error(e);
                return null;
               // return default(T);
            }
        }


        public  DataResult<RootobjectPlayList> GetAllPlayList(string access_token)
        {
            try
            {
                DataResult<RootobjectPlayList> dataResult = new DataResult<RootobjectPlayList>();

                DataResult<RootobjectUser> rootobjectuser = GetUser(access_token);

                if (!rootobjectuser.Success)
                {
                    dataResult.Success = false;
                    dataResult.Message = rootobjectuser.Message;

                    return dataResult;
                }


                string url = $"{DeezerAccessProperties.BaseUrl}user/{rootobjectuser.Data.id}/playlists";

                RootobjectPlayList rootobject = GetObjec<RootobjectPlayList>(url);

                if (rootobject.data != null)
                {
                    for (int i = 0; i < rootobject.data.Length; i++)
                    {
                        rootobject.data[i].TrackList = GetObjec<TrackList>(rootobject.data[i].tracklist);
                    }
                }
                dataResult.Data = rootobject;
                dataResult.Success = true;

                return dataResult;

                TrackList trackList = GetObjec<TrackList>(rootobject.data[0].tracklist);
            }
            catch (Exception ex)
            {
                return new DataResult<RootobjectPlayList>()
                {
                    Success = false,
                    Message = ex.Message
                };
            }
        }


        public  DataResult<CreatePlayList> CreatePlayList(string access_token, string title)
        {
            try
            {
                DataResult<CreatePlayList> dataResult = new DataResult<CreatePlayList>();
                DataResult<RootobjectUser> rootobjectuser = GetUser(access_token);

                if (!rootobjectuser.Success)
                {
                    dataResult.Success = false;
                    dataResult.Message = rootobjectuser.Message;
                    return dataResult;
                }

                string url = $"{DeezerAccessProperties.BaseUrl}user/{rootobjectuser.Data.id}/playlists?access_token={access_token}";

                DataResult<CreatePlayList> data = Post(url, title);




                return data;
            }
            catch(Exception ex)
            {
                return new DataResult<CreatePlayList>()
                {
                    Success = false,
                    Message = ex.Message
                };
            }

          
        }


        public class Test
        {
            public string title { get; set; }
        }
    }
}
