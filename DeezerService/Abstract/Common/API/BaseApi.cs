﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeezerService.Abstract.Common.API
{
    public abstract class BaseApi
    {
        public virtual T GetObjectAsync<T>(string url, string requestMethod = "GET", List<Cookie> cookies = null)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = requestMethod;

                if (cookies != null)
                {
                    if (request.CookieContainer == null)
                    {
                        request.CookieContainer = new CookieContainer();
                    }

                    foreach (var cookie in cookies)
                        request.CookieContainer.Add(cookie);
                }

                var httpResponse = request.GetResponse();

                using (var stream = httpResponse.GetResponseStream())
                {
                    StreamReader stremReder = new StreamReader(stream);
                    string json = stremReder.ReadToEnd();


                    stremReder.Dispose();

                    if (IsParseErrorData(json))
                    {
                        Logger.Logger.Log.Error(json);

                        return default(T);
                    }

                    return JsonConvert.DeserializeObject<T>(json);
                }

            }
            catch (Exception e)
            {
                Logger.Logger.Log.Error(e);

                return default(T);
            }
        }




        public virtual async Task<T> Post<T>(string json, string url)
        {
            try
            {
                //Logger.Logger.Log.Warning(json);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var httpResponse = httpWebRequest.GetResponse();

                using (var stream = httpResponse.GetResponseStream())
                {
                    StreamReader stremReder = new StreamReader(stream);
                    string json1 = stremReder.ReadToEnd();


                    stremReder.Dispose();

                    if (IsParseErrorData(json1))
                    {
                        Logger.Logger.Log.Error(json1);

                        return default(T);
                    }

                    return JsonConvert.DeserializeObject<T>(json1);
                }
            }
            catch (Exception e)
            {
                Logger.Logger.Log.Error(e);

                return default(T);
            }
        }

        public virtual T GetObjec<T>(string url, string requestMethod = "GET")
        {

            try
            {
                var request = WebRequest.Create(url);
                request.Method = requestMethod;

                var httpResponse = request.GetResponse();

                using (var stream = httpResponse.GetResponseStream())
                {
                    StreamReader stremReder = new StreamReader(stream);
                    string json = stremReder.ReadToEnd();


                    stremReder.Dispose();

                    if (IsParseErrorData(json))
                    {
                        Logger.Logger.Log.Error(json);

                        return default(T);
                    }

                    return JsonConvert.DeserializeObject<T>(json);
                }

            }
            catch (Exception e)
            {
                Logger.Logger.Log.Error(e);

                return default(T);
            }
        }

        public virtual void Send(string url)
        {
            try
            {
                var request = WebRequest.Create(url);
                request.Method = "GET";

                var httpResponse = request.GetResponse();
            }
            catch (Exception e)
            {
                Logger.Logger.Log.Error(e);
            }
        }

        protected virtual bool IsParseErrorData(string json)
        {
            try
            {
                var errorJson = JObject.Parse(json);
                var errorToken = errorJson["httpStatusCode"];

                return errorToken != null;
            }
            catch
            {
                return false;
            }
        }
    }
}
