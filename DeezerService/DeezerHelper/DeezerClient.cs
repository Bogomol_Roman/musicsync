﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeezerService.Models;
using E.Deezer;
using E.Deezer.Api;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Configuration;

namespace DeezerService.DeezerHelper
{
    public class DeezerClient
    {
        private static Deezer _deezerSession;
        private string _key;
        
        public DeezerClient(IConfiguration configuration)
        {
            _deezerSession = DeezerSession.CreateNew();
            _key = configuration["DeezerAppSettings:Token"];
            
            PerformUserLogin().GetAwaiter().GetResult();
        }

        private async Task PerformUserLogin()
        {
            await _deezerSession.Login(_key);
        }

        public async Task<IEnumerable<Track>> GetCurrentUserTracks()
        {
            var userTracks = await _deezerSession.Browse.CurrentUser.GetFavouriteTracks();

            return userTracks.Select(ModelConverter.ConvertExternalToInternalTrackModel)
                .ToList();
        }
    }
}